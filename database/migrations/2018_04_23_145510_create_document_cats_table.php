<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDocumentCatsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('document_Cats', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('maincat_id')->unsigned();
            $table->string('category');
            $table->string('doc_title');
            $table->string('doc_code');
            $table->string('doc_attach')->nullable('true');
            $table->string('remarks');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('document_Cats');
    }
}
