<?php

namespace App\Http\Controllers;

use App\Category;
use App\Document;
use App\main_category;
use App\sub_category;
use Illuminate\Http\Request;


class listController extends Controller
{
    public function index()
    {
    	$maincats = main_category::all();
    	$categories = Category::all();
    	$subcats = sub_category::all();
    	$documents = Document::all();
    	return view('home', compact('maincats', 'categories', 'subcats', 'documents'));
    	//$category = Category::where('category', 'Quality Manual')->first(); return $category;
    }

    public function create(Request $request)
    {
    	// $Document = new Category;
    	// $Document->category = "test";
    	// $Document->save();
    	// return $request->text;

    	// $subcat = new sub_category;
    	// $category = Category::where('category', 'Quality Manual')->first();
    	// $subcat->cat_id = $category->id;
    	// $subcat->subcategory = "QM-02";
    	// $subcat->save();
    	// return  $subcat->subcategory.' Inserted';

    	$maincat = new main_category;
    	$maincat->main_category = "Evidence of ISO 9001 - Aligned QMS Implementation";
    	$maincat->save();
    	return $maincat->main_category. ' Inserted';
    }

    public function dic()
    {
        return view('dic');
    }
}
