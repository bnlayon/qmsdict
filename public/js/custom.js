/*
* Custom Javascript
* By: Ryan James A. Caday
*
*/

$(document).ready(function() {
			$('.card-body').removeClass('card-body');

			$("#accordionEx [data-toggle=collapse]:last").click();

			$('.ourItem').each(function() {
				$(this).click(function(event) {
					var text = $(this).text();
					$.post('list', {'text': text,'_token':$('input[name=_token]').val()}, function(data) {
						console.log(data);
					});
				});
			});
		});
