<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="Quality Management System">
	<meta name="author" content="Ryan James A. Caday, Bernard N Layon">

	<title>QMS - @yield('title')</title>

	<!-- Bootstrap core CSS -->
	<link href="{{asset('css/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="{{asset('css/scrolling-nav.css')}}" rel="stylesheet">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

	<link href="{{asset('css/custom.css')}}" rel="stylesheet">

</head>

<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		<div class="container">
			<a class="navbar-brand js-scroll-trigger" href="#page-top">NEDA-QMS Beta</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">
				<ul class="navbar-nav ml-auto">
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="{{asset('')}}">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link js-scroll-trigger" href="{{asset('/dic')}}">Documented Information Control</a>
					</li>
				</ul>
			</div>
		</div>
	</nav>

	@yield('content')

	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; NEDA Information and Communications Technology Staff 2018</p>
		</div>
		<!-- /.container -->
	</footer>
	{{csrf_field()}}
	<!-- Jquery CDN -->
	{{-- <script
		src="http://code.jquery.com/jquery-3.3.1.min.js"
		integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
		crossorigin="anonymous"></script> --}}
		<script src="{{asset('js/jqv3.3.1.js')}}"></script>

		<!-- Bootstrap core JavaScript -->
		<script src="{{asset('js/bootstrap/jquery/jquery.min.js')}}"></script>
		<script src="{{asset('js/bootstrap/js/bootstrap.bundle.min.js')}}"></script>

		<!-- Plugin JavaScript -->
		<script src="{{asset('js/bootstrap/jquery-easing/jquery.easing.min.js')}}"></script>

		<!-- Custom JavaScript for this theme -->
		<script src="{{asset('js/scrolling-nav.js')}}"></script>
		<script src="{{asset('js/custom.js')}}"></script>


	</body>

	</html>