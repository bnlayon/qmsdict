<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="Ryan James A. Caday">

  <title>Scrolling Nav - Start Bootstrap Template</title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom styles for this template -->
  <link href="css/scrolling-nav.css" rel="stylesheet">

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
    <div class="container">
      <a class="navbar-brand js-scroll-trigger" href="#page-top">Start Bootstrap</a>
      <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav ml-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#about">About</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#services">Services</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" href="#contact">Contact</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>

  <header class="bg-primary text-white">
    <div class="container text-center">
      <h1>Welcome to Scrolling Nav</h1>
      <p class="lead">A landing page template freshly redesigned for Bootstrap 4</p>
    </div>
  </header>

  <section id="about">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2>About this page</h2>
          <p class="lead">This is a great place to talk about your webpage. This template is purposefully unstyled so you can use it as a boilerplate or starting point for you own landing page designs! This template features:</p>
          <ul>
            <li>Clickable nav links that smooth scroll to page sections</li>
            <li>Responsive behavior when clicking nav links perfect for a one page website</li>
            <li>Bootstrap's scrollspy feature which highlights which section of the page you're on in the navbar</li>
            <li>Minimal custom CSS so you are free to explore your own unique design options</li>
          </ul>
        </div>
        <div class="col-lg-8 mx-auto">
         <div class="list-group">
           <a href="#" class="list-group-item ourItem">
             Cras justo odio
           </a>
           <a href="#" class="list-group-item ourItem">Dapibus ac facilisis in</a>
           <a href="#" class="list-group-item ourItem">Morbi leo risus</a>
           <a href="#" class="list-group-item ourItem">Porta ac consectetur ac</a>
           <a href="#" class="list-group-item ourItem">Vestibulum at eros</a>
         </div>
       </div>
     </div>
   </div>
 </section>

 <section id="services" class="bg-light">
  <div class="container">
    <div class="row">
      <div class="col-lg-8 mx-auto">
        <h2>Services we offer</h2>

{{--             <div id="accordion">
            @foreach ($categories as $category)

              <div class="card">
                <div class="card-header" id="{{ $category->id }}">
                  <h5 class="mb-0">
                    <button class="btn btn-link collapsed" data-toggle="collapse" data-target="# {{ $category->id }}" aria-expanded="true" aria-controls="{{ $category->id }}">
                      {{ $category->category }}
                    </button>
                  </h5>
                </div>

                <div id=" {{ $category->id }}" class="collapse" aria-labelledby="{{ $category->id }}" data-parent="#accordion">
                  <div class="card-body">
                    test1
                  </div>
                </div>
              </div>
            @endforeach
          </div> --}}
          {{-- end of accordion --}}
        </div>
      </div>
    </div>
  </section>

  <section id="contact">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2>Contact us</h2>

          <!--Main Category Accordion wrapper-->
          <div class="accordion" id="accordionEx0" role="tablist" aria-multiselectable="true">
            @foreach ($maincats as $maincat)
            <!-- Main Category Accordion card -->
            <div class="card">

              <!-- Main Category Card header -->
              <div class="card-header" role="tab" id="heading1">
                <a data-toggle="collapse" data-parent="#accordionEx0" href="#{{$maincat->main_category}}" aria-expanded="true" aria-controls="collapseOne">
                  <h5 class="mb-0">
                    {{$maincat->main_category}} <i class="fa fa-angle-down rotate-icon"></i>
                  </h5>
                </a>
              </div>

              <!-- Main Category Card body -->
              <div id="{{$maincat->main_category}}" class="collapse show" role="tabpanel" aria-labelledby="heading1" data-parent="#accordionEx0">
                <div class="card-body">

                  <!--Category Accordion wrapper-->
                  <div class="accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
                    @foreach ($categories as $category)
                    @if ($category->maincat_id === $maincat->id)
                    <!-- Category Accordion card -->
                    <div class="card">
                      <!-- Category Card header -->
                      <div class="card-header" role="tab" id="headingOne">
                        <a data-toggle="collapse" data-parent="#accordionEx" href="#{{$category->category}}" aria-expanded="true" aria-controls="collapseOne">
                          <h5 class="mb-0">
                            <li>{{$category->category}} <i class="fa fa-angle-down rotate-icon"></i></li>
                          </h5>
                        </a>
                      </div>

                      @foreach ($subcats as $subcat)
                      @if ($subcat->cat_id === $category->id)

                      <!-- Category Card body -->
                      <div id="{{$category->category}}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionEx">
                        <div class="card-body">

                          <!--Sub Cat Accordion wrapper-->
                          <div class="accordion" id="accordionEx2" role="tablist" aria-multiselectable="true">
                            <!-- Sub Cat Accordion card -->
                            <div class="card">
                              <!-- Sub Cat Card header -->
                              <div class="card-header" role="tab" id="headingOne">
                                <a data-toggle="collapse" data-parent="#accordionEx2" href="#{{$subcat->subcategory}}" aria-expanded="true" aria-controls="collapseOne">
                                  <h5 class="mb-0">
                                    {{$subcat->subcategory}}
                                  </h5>
                                </a>
                              </div>
                              <!-- Sub Cat Card body -->
                              <div id="{{$subcat->subcategory}}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionEx2">
                                <div class="card-body">
                                  Body 1
                                </div>
                              </div>
                            </div>
                            <!-- /. Sub Cat Accordion card -->                                           
                          </div>
                          <!--/. Sub Cat Accordion wrapper-->

                        </div>
                      </div>
                      <!--/. Category Card body -->

                      @endif
                      @endforeach

                    </div>
                    <!-- Category Accordion card -->
                    @endif
                    @endforeach    
                  </div>
                  <!--/.Category Accordion wrapper-->


                </div>
              </div>
              <!-- Main Category Card body -->
            </div>
            <!-- Main Category Accordion card -->
            @endforeach    
          </div>
          <!--/.Main Category Accordion wrapper-->

          


        </div>
      </div>
    </div>
  </section>

  <!-- Footer -->
  <footer class="py-5 bg-dark">
    <div class="container">
      <p class="m-0 text-center text-white">Copyright &copy; Your Website 2017</p>
    </div>
    <!-- /.container -->
  </footer>
  {{csrf_field()}}
  <!-- Jquery CDN -->
  <script
  src="http://code.jquery.com/jquery-3.3.1.min.js"
  integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
  crossorigin="anonymous"></script>

  <!-- Bootstrap core JavaScript -->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom JavaScript for this theme -->
  <script src="js/scrolling-nav.js"></script>

  <script type="text/javascript">
   $(document).ready(function() {
    $('.ourItem').each(function() {
      $(this).click(function(event) {
        var text = $(this).text();
        $.post('list', {'text': text,'_token':$('input[name=_token]').val()}, function(data) {
          console.log(data);
        });
      });
    });
  });
</script>

</body>

</html>
