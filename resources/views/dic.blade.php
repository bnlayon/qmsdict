@extends('layout')

@section('title', 'Documented Information Control')

@section('content')

<section id="dic">
<!-- Page Content -->
<div class="container">

  <div class="row">

    <div class="col-lg-3">
      <h1 class="my-4">Documented Information Control</h1>
      <div class="list-group">
        <a href="#" class="list-group-item active">How to move Documented Information (DI) ?</a>
      </div>
    </div>
    <!-- /.col-lg-3 -->

    <div class="col-lg-9">
      <h1 class="my-4">How to move Documented Information (DI) ?</h1>
      <h6 class="my-4"><a href="http://tinyurl.com/dimrform" target="_blank">Download Documented Information Movement Request Form (DIMRF)</a></h6>
      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 1: Type of Movement</b>
        </div>
        <div class="card-body1">
          <p>Tick the box that corresponds to your desired Document Information Movement.</p>
          <img class="card-img-top img-fluid" src="dict_images/1.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 2: Date of Request</b>
        </div>
        <div class="card-body1">
          <p>Indicate the date of request following this format: MM/DD/YYYY</p>
          <img class="card-img-top img-fluid" src="dict_images/2.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 3: Reason of Request</b>
        </div>
        <div class="card-body1">
          <p>For Creation: Identify the reason for creation of new DI.</p>
          <p>For Revisions: Identify the specific changes being requested.</p>
          <p>For Deletion: Identify the reason for deletion.</p>
          <img class="card-img-top img-fluid" src="dict_images/3.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 4: Document Title</b>
        </div>
        <div class="card-body1">
          <p>Determine the complete title of the DI being moved.</p>
          <img class="card-img-top img-fluid" src="dict_images/4.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 5: Document Code</b>
        </div>
        <div class="card-body1">
          <p>Define the correct code of the DI being moved.</p>
          <small>Note: If unsure of the document code, information may be verified by the DIC Team.</small>
          <img class="card-img-top img-fluid" src="dict_images/5.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 6: Revision Level</b>
        </div>
        <div class="card-body1">
          <p>Establish the revision level from which it is being escalated to.</p>
          <small>Note: If unsure of the revision level, information may be verified by the DIC Team.</small>
          <img class="card-img-top img-fluid" src="dict_images/6.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 7: Effectivity Date</b>
        </div>
        <div class="card-body1">
          <p>Indicate the proposed effectivity date of the DI being moved: MM/DD/YYYY</p>
          <img class="card-img-top img-fluid" src="dict_images/7.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 8: Signatories</b>
        </div>
        <div class="card-body1">
          <p>Refer to table below for the appropriate signatory for each field.</p>
          <img class="card-img-top img-fluid" src="dict_images/8.png" alt="">
          <img class="card-img-top img-fluid" src="dict_images/9.png" alt="">
        </div>
      </div>

      <div class="card card-outline-secondary my-4">
        <div class="card-header">
          <b>Step 9: Submission</b>
        </div>
        <div class="card-body1">
          <p>Submit the DIMRF to ICTS together with the DI being moved.</p>
          <small>Note: The process owner must provide a receiving copy so that it will be appropriately labeled.</small>
        </div>
      </div>
      <!-- /.card -->

    </div>
    <!-- /.col-lg-9 -->

  </div>

</div>
<!-- /.container -->

</section>

@endsection