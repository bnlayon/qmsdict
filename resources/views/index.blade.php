<!DOCTYPE html>
<html lang="en">

<head>

	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">

	<title>QMS - Quality Management System</title>

	<!-- Bootstrap core CSS -->
	<link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

	<!-- Custom styles for this template -->
	<link href="css/scrolling-nav.css" rel="stylesheet">
	<link rel="shortcut icon" href="{{ asset('favicon.ico') }}">

	<style>
		a:link
		{
		    color: #007bff; 
		    text-decoration: none;
		}
		li
		{
			color: black !important; 
		    text-decoration: none !important;

		}
		#cp
		{
			color: #007bff !important; 
		    text-decoration: none !important;

		}
		.innerSubCat {
			font-size: 12px;

		}

	</style>

</head>

<body id="page-top">

	<!-- Navigation -->
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top" id="mainNav">
		<div class="container">
			<a class="navbar-brand js-scroll-trigger" href="#page-top">NEDA-QMS Beta</a>
			<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
				<span class="navbar-toggler-icon"></span>
			</button>
			<div class="collapse navbar-collapse" id="navbarResponsive">

			</div>
		</div>
	</nav>

	<header class="bg-primary text-white">
		<div class="container text-center">
			{{-- <h1>Republic of the Philippines</h1> --}}
			<h1>National Economic and Development Authority</h1>
			<h5>Quality Management System (QMS) </br>Investment Coordication Committee (ICC) Secretariat Appraisal and Facilitation of ICC Action</h5>
			{{-- <p class="lead">Investment Coordication Committee (ICC) Secretariat Appraisal and Facilitation of ICC Action</p> --}}
		</div>
	</header>

	@foreach ($maincats as $maincat)
	<section id="contact">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 mx-auto">
					<h2 class="text-center">{{$maincat->main_category}}</h2>

					<!--Category Accordion wrapper-->
					<div class="accordion" id="accordionEx" role="tablist" aria-multiselectable="true">
						@foreach ($categories as $category)
						@if ($category->maincat_id === $maincat->id)
						<!-- Category Accordion card -->
						<div class="card">
							<!-- Category Card header -->
							<div class="card-header" role="tab" id="headingOne">
								<a data-toggle="collapse" data-parent="#accordionEx" href="#{{$category->category}}" aria-expanded="true" aria-controls="collapseOne">
									<h6 class="mb-0">
										@if (!is_null($category->doc_attach))
										<a href="{{$category->doc_attach}}" target="_blank">{{$category->category}} <i class="fa fa-angle-down rotate-icon"></i></a>
										@else
										{{$category->category}} <i class="fa fa-angle-down rotate-icon"></i>
										@endif
									</h6>
								</a>
							</div>

							<!-- Category Card body -->
							<div id="{{$category->category}}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionEx">
								<div class="card-body">

									<!--Sub Cat Accordion wrapper-->
									<div class="accordion" id="accordion3" role="tablist" aria-multiselectable="true">
										@foreach ($subcats as $subcat)
										@if ($subcat->cat_id === $category->id)
										<!-- Accordion card -->
										<div class="card">
											<!-- Card header -->
											<div class="card-header" role="tab" id="headingTwo">
												<a class="collapsed" data-toggle="collapse" data-parent="#accordion3" href="# {{$subcat->subcategory}}" aria-expanded="false" aria-controls="collapseTwo">
													<h6 class="mb-0">
														<li id="cp">{{$subcat->subcategory}} <i class="fa fa-angle-down rotate-icon"></i></li>
													</h6>
												</a>
											</div>
											<!-- Card body -->
											<div id=" {{$subcat->subcategory}}" class="collapse" role="tabpanel" aria-labelledby="headingTwo" data-parent="#accordion3">
												<div class="card-body">

													<!--Sub Cat Inner Accordion wrapper-->
													<div class="accordion" id="accordion4" role="tablist" aria-multiselectable="true">
														@foreach ($documents as $document)
														@if ($document->doc_sub_cat === $subcat->id)
														<!-- Accordion card -->
														<div class="card">
															<!-- Card header -->
															<div class="card-header" role="tab" id="hTwo">
																<a class="collapsed" data-toggle="collapse" data-parent="#accordion4" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
																	<h6 class="mb-0 px-3">
																		@if (!is_null($document->attach))
																		<a href="{{$document->attach}}" target="_blank"><li class="innerSubCat">{{$document->doc_title}}</li></a>
																		@else
																		<li class="innerSubCat">{{$document->doc_title}}</li>
																		@endif
																	</h6>
																</a>
															</div>
															<!-- Card body -->
															<div id="collapseTwo" class="collapse" role="tabpanel" aria-labelledby="hTwo" data-parent="#accordion4">
																<div class="card-body">
																</div>
															</div>
														</div>
														<!-- Accordion card -->
														@endif
														@endforeach
													</div>
													<!--/.Sub Cat Inner Accordion wrapper-->

												</div>
											</div>
										</div>
										<!-- Accordion card -->
										@endif
										@endforeach
									</div>
									<!--/.Sub Cat Accordion wrapper-->

									<!--Document Accordion wrapper-->
									<div class="accordion" id="accordionEx2" role="tablist" aria-multiselectable="true">
										@foreach ($documents as $document)
										@if($document->doc_cat === $category->id AND $document->doc_sub_cat === 0)
										<!-- Document Accordion card -->
										<div class="card">
											<!-- Document Card header -->
											<div class="card-header" role="tab" id="headingOne">
												<a data-toggle="collapse" data-parent="#accordionEx2" href="#{{$document->doc_title}}" aria-expanded="true" aria-controls="collapseOne">
													<h6 class="mb-0 px-2" style="font-size:12px;">
														@if (!is_null($document->attach))
														<a href="{{$document->attach}}" target="_blank"><li>{{$document->doc_title}}</li></a>
														@else
														<li>{{$document->doc_title}}</li>
														@endif
													</h6>
												</a>
											</div>
											<!-- Document Card body -->
											<div id="{{$subcat->subcategory}}" class="collapse" role="tabpanel" aria-labelledby="headingOne" data-parent="#accordionEx2">
												<div class="card-body">
												</div>
											</div>
										</div><!-- /. Document Accordion card -->                                           
										@endif
										@endforeach
									</div><!--/. Document Accordion wrapper-->
									
								</div>
							</div>
							<!--/. Category Card body -->

						</div>
						<!-- Category Accordion card -->
						@endif
						@endforeach    
					</div>
					<!--/.Category Accordion wrapper-->


				</div>
			</div>
		</div>
	</section>
	@endforeach

	<!-- Footer -->
	<footer class="py-5 bg-dark">
		<div class="container">
			<p class="m-0 text-center text-white">Copyright &copy; NEDA Information and Communications Technology Staff 2018</p>
		</div>
		<!-- /.container -->
	</footer>
	{{csrf_field()}}
	<!-- Jquery CDN -->
	{{-- <script
	src="http://code.jquery.com/jquery-3.3.1.min.js"
	integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8="
	crossorigin="anonymous"></script> --}}
	<script src="js/jqv3.3.1.js"></script>

	<!-- Bootstrap core JavaScript -->
	<script src="vendor/jquery/jquery.min.js"></script>
	<script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

	<!-- Plugin JavaScript -->
	<script src="vendor/jquery-easing/jquery.easing.min.js"></script>

	<!-- Custom JavaScript for this theme -->
	<script src="js/scrolling-nav.js"></script>
	<script src="js/custom.js"></script>


</body>

</html>
