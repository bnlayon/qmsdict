-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Apr 27, 2018 at 06:09 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `qmsprojdb`
--

-- --------------------------------------------------------

--
-- Table structure for table `cms_apicustom`
--

CREATE TABLE `cms_apicustom` (
  `id` int(10) UNSIGNED NOT NULL,
  `permalink` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `tabel` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `aksi` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `kolom` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `orderby` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sub_query_1` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sql_where` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `nama` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `keterangan` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameter` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `method_type` varchar(25) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parameters` longtext COLLATE utf8mb4_unicode_ci,
  `responses` longtext COLLATE utf8mb4_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_apikey`
--

CREATE TABLE `cms_apikey` (
  `id` int(10) UNSIGNED NOT NULL,
  `screetkey` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `hit` int(11) DEFAULT NULL,
  `status` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'active',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_dashboard`
--

CREATE TABLE `cms_dashboard` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_queues`
--

CREATE TABLE `cms_email_queues` (
  `id` int(10) UNSIGNED NOT NULL,
  `send_at` datetime DEFAULT NULL,
  `email_recipient` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email_content` text COLLATE utf8mb4_unicode_ci,
  `email_attachments` text COLLATE utf8mb4_unicode_ci,
  `is_sent` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_email_templates`
--

CREATE TABLE `cms_email_templates` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `subject` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `from_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cc_email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_email_templates`
--

INSERT INTO `cms_email_templates` (`id`, `name`, `slug`, `subject`, `content`, `description`, `from_name`, `from_email`, `cc_email`, `created_at`, `updated_at`) VALUES
(2, 'Email Template Forgot Password Backend', 'forgot_password_backend', 'Password Recovery', '<p>Hi,</p><p>Someone requested forgot password, here is your new password : </p><p>[password]</p><p><br></p><p>--</p><p>Regards,</p><p>Admin</p>', '[password]', 'System', 'SysAdmin@neda.gov.ph', '', '2018-04-22 04:53:56', '2018-04-24 21:06:25');

-- --------------------------------------------------------

--
-- Table structure for table `cms_logs`
--

CREATE TABLE `cms_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `ipaddress` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `useragent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_logs`
--

INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(1, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-19 21:49:26', NULL),
(2, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/module_generator/delete/13', 'Delete data Document Category at Module Generator', 1, '2018-04-19 22:28:15', NULL),
(3, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 1 at Document Category', 1, '2018-04-19 22:29:02', NULL),
(4, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 2 at Document Category', 1, '2018-04-19 22:30:37', NULL),
(5, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 3 at Document Category', 1, '2018-04-19 22:30:52', NULL),
(6, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 4 at Document Category', 1, '2018-04-19 22:31:12', NULL),
(7, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 5 at Document Category', 1, '2018-04-19 22:31:26', NULL),
(8, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 6 at Document Category', 1, '2018-04-19 22:31:44', NULL),
(9, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 7 at Document Category', 1, '2018-04-19 22:32:38', NULL),
(10, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-19 22:32:52', NULL),
(11, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/module_generator/delete/13', 'Delete data Document Category at Module Generator', 1, '2018-04-19 22:33:15', NULL),
(12, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/module_generator/delete/12', 'Delete data Documents at Module Generator', 1, '2018-04-19 22:33:26', NULL),
(13, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/documents/add-save', 'Add New Data Quality Manual 01 at Documents', 1, '2018-04-19 23:00:22', NULL),
(14, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/users/add-save', 'Add New Data Ryan James Caday at Users', 1, '2018-04-19 23:07:42', NULL),
(15, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-19 23:07:51', NULL),
(16, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-19 23:08:03', NULL),
(17, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-19 23:28:34', NULL),
(18, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-19 23:28:44', NULL),
(19, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/menu_management/add-save', 'Add New Data Documents at Menu Management', 1, '2018-04-20 00:24:16', NULL),
(20, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/menu_management/delete/2', 'Delete data Documents at Menu Management', 1, '2018-04-20 00:25:00', NULL),
(21, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:25:28', NULL),
(22, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:25:53', NULL),
(23, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-20 00:26:03', NULL),
(24, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:26:16', NULL),
(25, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/menu_management/add-save', 'Add New Data Documents at Menu Management', 1, '2018-04-20 00:27:56', NULL),
(26, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/menu_management/delete/4', 'Delete data Documents at Menu Management', 1, '2018-04-20 00:28:00', NULL),
(27, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:29:24', NULL),
(28, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:29:37', NULL),
(29, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-20 00:29:57', NULL),
(30, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:30:19', NULL),
(31, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:32:30', NULL),
(32, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:33:14', NULL),
(33, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:47:25', NULL),
(34, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:47:53', NULL),
(35, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/edit/1', 'Try edit the data Quality Manual 01 at Documents', 2, '2018-04-20 00:49:38', NULL),
(36, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/edit/1', 'Try edit the data Quality Manual 01 at Documents', 2, '2018-04-20 00:49:40', NULL),
(37, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/edit/1', 'Try edit the data Quality Manual 01 at Documents', 2, '2018-04-20 00:49:42', NULL),
(38, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/edit/1', 'Try edit the data Quality Manual 01 at Documents', 2, '2018-04-20 00:49:43', NULL),
(39, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/edit/1', 'Try edit the data Quality Manual 01 at Documents', 2, '2018-04-20 00:49:44', NULL),
(40, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/edit/1', 'Try edit the data Quality Manual 01 at Documents', 2, '2018-04-20 00:49:48', NULL),
(41, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/edit/1', 'Try edit the data Quality Manual 01 at Documents', 2, '2018-04-20 00:49:50', NULL),
(42, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'rcaday0@gmail.com logout', 2, '2018-04-20 00:49:54', NULL),
(43, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:50:15', NULL),
(44, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:50:46', NULL),
(45, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:51:01', NULL),
(46, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:01', NULL),
(47, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:01', NULL),
(48, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:02', NULL),
(49, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:02', NULL),
(50, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:02', NULL),
(51, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:03', NULL),
(52, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:03', NULL),
(53, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:51:03', NULL),
(54, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:03', NULL),
(55, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:04', NULL),
(56, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:04', NULL),
(57, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:04', NULL),
(58, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:05', NULL),
(59, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:05', NULL),
(60, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:05', NULL),
(61, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:06', NULL),
(62, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:06', NULL),
(63, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:06', NULL),
(64, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:11', NULL),
(65, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:12', NULL),
(66, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:12', NULL),
(67, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:12', NULL),
(68, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:13', NULL),
(69, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:13', NULL),
(70, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:13', NULL),
(71, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:14', NULL),
(72, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:14', NULL),
(73, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:14', NULL),
(74, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:15', NULL),
(75, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:15', NULL),
(76, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:15', NULL),
(77, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:15', NULL),
(78, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:16', NULL),
(79, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:16', NULL),
(80, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:16', NULL),
(81, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:17', NULL),
(82, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:17', NULL),
(83, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:17', NULL),
(84, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:18', NULL),
(85, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:18', NULL),
(86, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:23', NULL),
(87, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:24', NULL),
(88, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:24', NULL),
(89, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:24', NULL),
(90, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:25', NULL),
(91, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:25', NULL),
(92, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:25', NULL),
(93, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:25', NULL),
(94, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:26', NULL),
(95, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:26', NULL),
(96, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:26', NULL),
(97, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:51:48', NULL),
(98, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:48', NULL),
(99, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:49', NULL),
(100, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:49', NULL),
(101, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:49', NULL),
(102, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:50', NULL),
(103, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:50', NULL),
(104, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:50', NULL),
(105, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:51', NULL),
(106, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:51', NULL),
(107, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents', 'Try view the data :name at Documents', 2, '2018-04-20 00:51:51', NULL),
(108, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:52:07', NULL),
(109, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:52:32', NULL),
(110, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:52:54', NULL),
(111, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'rcaday0@gmail.com logout', 2, '2018-04-20 00:55:34', NULL),
(112, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:55:50', NULL),
(113, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/delete/7', 'Delete data Documents at Menu Management', 1, '2018-04-20 00:56:03', NULL),
(114, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:56:08', NULL),
(115, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:56:20', NULL),
(116, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/edit/8', 'Try edit the data 8 at Document Category', 2, '2018-04-20 00:56:36', NULL),
(117, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/edit/8', 'Try edit the data 8 at Document Category', 2, '2018-04-20 00:56:43', NULL),
(118, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'rcaday0@gmail.com logout', 2, '2018-04-20 00:56:48', NULL),
(119, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:57:03', NULL),
(120, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@crudbooster.com logout', 1, '2018-04-20 00:57:34', NULL),
(121, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-20 00:57:47', NULL),
(122, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'rcaday0@gmail.com logout', 2, '2018-04-20 00:58:48', NULL),
(123, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-20 00:59:09', NULL),
(124, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/action-selected', 'Delete data 8 at Document Category', 1, '2018-04-20 01:04:05', NULL),
(125, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-20 01:08:57', NULL),
(126, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/delete/8', 'Delete data 8 at Document Category', 1, '2018-04-20 01:11:05', NULL),
(127, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-20 01:11:10', NULL),
(128, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/notifications/delete/2', 'Delete data Hellow World at Notifications', 1, '2018-04-20 01:12:59', NULL),
(129, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/notifications/delete/1', 'Delete data Hellow World at Notifications', 1, '2018-04-20 01:13:02', NULL),
(130, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/delete/8', 'Delete data 8 at Document Category', 1, '2018-04-20 01:13:30', NULL),
(131, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-20 01:13:35', NULL),
(132, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-20 01:22:53', NULL),
(133, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 11 at Document Category', 1, '2018-04-20 01:25:45', NULL),
(134, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 14 at Document Category', 1, '2018-04-20 01:31:04', NULL),
(135, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/action-selected', 'Delete data 14,13,12,11,10 at Document Category', 1, '2018-04-20 01:31:27', NULL),
(136, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 10 at Document Category', 1, '2018-04-20 01:31:37', NULL),
(137, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/notifications/action-selected', 'Delete data 7,6,5,4,3 at Notifications', 1, '2018-04-20 01:32:14', NULL),
(138, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/delete/10', 'Delete data 10 at Document Category', 1, '2018-04-20 01:32:26', NULL),
(139, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 10 at Document Category', 1, '2018-04-20 01:32:30', NULL),
(140, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 11 at Document Category', 1, '2018-04-20 01:33:43', NULL),
(141, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 12 at Document Category', 1, '2018-04-20 01:34:56', NULL),
(142, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 13 at Document Category', 1, '2018-04-20 01:35:20', NULL),
(143, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 15 at Document Category', 1, '2018-04-20 01:38:00', NULL),
(144, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/action-selected', 'Delete data 15,14 at Document Category', 1, '2018-04-20 01:38:43', NULL),
(145, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/notifications/action-selected', 'Delete data 12,11,10,9,8 at Notifications', 1, '2018-04-20 01:38:51', NULL),
(146, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 14 at Document Category', 1, '2018-04-20 01:39:01', NULL),
(147, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 15 at Document Category', 1, '2018-04-20 01:40:45', NULL),
(148, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 17 at Document Category', 1, '2018-04-20 01:44:16', NULL),
(149, '::1', 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cat/add-save', 'Add New Data 18 at Document Category', 1, '2018-04-20 01:44:56', NULL),
(150, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-22 04:54:45', NULL),
(151, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 19,18,17,16,15,14,13,12,11,10 at Document Category', 1, '2018-04-22 04:57:20', NULL),
(152, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 9,8 at Document Category', 1, '2018-04-22 04:57:27', NULL),
(153, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-22 04:57:43', NULL),
(154, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-22 05:02:19', NULL),
(155, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/notifications/action-selected', 'Delete data 18,17,16,15,14,13 at Notifications', 1, '2018-04-22 05:02:46', NULL),
(156, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 9,8 at Document Category', 1, '2018-04-22 05:03:02', NULL),
(157, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-22 05:07:32', NULL),
(158, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-22 05:07:44', NULL),
(159, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 2, '2018-04-22 05:07:51', NULL),
(160, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-22 05:07:57', NULL),
(161, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-22 05:08:11', NULL),
(162, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-22 05:10:21', NULL),
(163, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/notifications/action-selected', 'Delete data 20,19 at Notifications', 1, '2018-04-22 05:14:59', NULL),
(164, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 9,8 at Document Category', 1, '2018-04-22 05:15:07', NULL),
(165, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-22 05:15:14', NULL),
(166, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-22 05:17:20', NULL),
(167, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/notifications/action-selected', 'Delete data 22,21 at Notifications', 1, '2018-04-22 05:18:09', NULL),
(168, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 9,8 at Document Category', 1, '2018-04-22 05:18:16', NULL),
(169, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 10,9,8 at Document Category', 1, '2018-04-22 05:43:07', NULL),
(170, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-22 05:43:41', NULL),
(171, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 9,8 at Document Category', 1, '2018-04-22 05:47:17', NULL),
(172, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 10 at Document Category', 1, '2018-04-22 05:48:28', NULL),
(173, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/notifications/action-selected', 'Delete data 2,1 at Notifications', 1, '2018-04-22 05:49:07', NULL),
(174, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 10,9,8 at Document Category', 1, '2018-04-22 05:49:13', NULL),
(175, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-22 05:49:22', NULL),
(176, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-22 05:49:36', NULL),
(177, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-22 05:49:46', NULL),
(178, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-22 05:53:21', NULL),
(179, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 11 at Document Category', 1, '2018-04-22 05:55:18', NULL),
(180, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 11,10,9,8 at Document Category', 1, '2018-04-22 05:57:01', NULL),
(181, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-22 05:57:05', NULL),
(182, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 8 at Document Category', 1, '2018-04-22 06:02:25', NULL),
(183, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/notifications/action-selected', 'Delete data 6,5,4,3 at Notifications', 1, '2018-04-22 06:02:33', NULL),
(184, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-22 06:02:46', NULL),
(185, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-22 06:03:30', NULL),
(186, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 10 at Document Category', 1, '2018-04-22 06:04:12', NULL),
(187, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 11 at Document Category', 1, '2018-04-22 06:05:37', NULL),
(188, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/notifications/action-selected', 'Delete data 10,9,8,7 at Notifications', 1, '2018-04-22 06:05:44', NULL),
(189, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 12 at Document Category', 1, '2018-04-22 06:05:56', NULL),
(190, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 16 at Document Category', 1, '2018-04-22 06:16:52', NULL),
(191, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 17 at Document Category', 1, '2018-04-22 06:17:11', NULL),
(192, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 17,16,15,14 at Document Category', 1, '2018-04-22 06:20:07', NULL),
(193, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 13,12,11,10,9,8 at Document Category', 1, '2018-04-22 06:20:19', NULL);
INSERT INTO `cms_logs` (`id`, `ipaddress`, `useragent`, `url`, `description`, `id_cms_users`, `created_at`, `updated_at`) VALUES
(194, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-22 06:20:23', NULL),
(195, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 9 at Document Category', 1, '2018-04-22 06:21:22', NULL),
(196, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/notifications/action-selected', 'Delete data 15,14,13,12,11 at Notifications', 1, '2018-04-22 06:21:33', NULL),
(197, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 11 at Document Category', 1, '2018-04-22 06:27:41', NULL),
(198, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 12 at Document Category', 1, '2018-04-22 06:28:40', NULL),
(199, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/action-selected', 'Delete data 12,11,10,9,8 at Document Category', 1, '2018-04-22 06:29:29', NULL),
(200, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cat/add-save', 'Add New Data 8 at Document Category', 1, '2018-04-22 06:29:52', NULL),
(201, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 00:56:02', NULL),
(202, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/module_generator/delete/12', 'Delete data Document Category at Module Generator', 1, '2018-04-24 00:56:30', NULL),
(203, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 1 at Document Category', 1, '2018-04-24 01:02:17', NULL),
(204, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 2 at Document Category', 1, '2018-04-24 01:04:41', NULL),
(205, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 3 at Document Category', 1, '2018-04-24 01:05:14', NULL),
(206, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 4 at Document Category', 1, '2018-04-24 01:05:36', NULL),
(207, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 5 at Document Category', 1, '2018-04-24 01:06:47', NULL),
(208, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 6 at Document Category', 1, '2018-04-24 01:07:13', NULL),
(209, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/menu_management/edit-save/9', 'Update data Category at Menu Management', 1, '2018-04-24 01:15:13', NULL),
(210, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-24 01:24:54', NULL),
(211, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 05:40:54', NULL),
(212, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/edit-save/2', 'Update data  at Sub-Categories', 1, '2018-04-24 05:41:18', NULL),
(213, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 3 at Sub-Categories', 1, '2018-04-24 05:48:21', NULL),
(214, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 4 at Sub-Categories', 1, '2018-04-24 05:48:32', NULL),
(215, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 5 at Sub-Categories', 1, '2018-04-24 05:48:45', NULL),
(216, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 6 at Sub-Categories', 1, '2018-04-24 05:49:00', NULL),
(217, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 17:21:42', NULL),
(218, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 7 at Sub-Categories', 1, '2018-04-24 17:22:09', NULL),
(219, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 8 at Sub-Categories', 1, '2018-04-24 17:22:20', NULL),
(220, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 9 at Sub-Categories', 1, '2018-04-24 17:22:51', NULL),
(221, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 19:52:43', NULL),
(222, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-24 20:32:38', NULL),
(223, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 20:33:12', NULL),
(224, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/email_templates/delete/1', 'Delete data Email Template Forgot Password Backend at Email Template', 1, '2018-04-24 20:39:20', NULL),
(225, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Backend at Email Template', 1, '2018-04-24 20:39:59', NULL),
(226, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-24 20:40:44', NULL),
(227, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 20:41:35', NULL),
(228, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-24 21:01:22', NULL),
(229, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 21:05:40', NULL),
(230, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/email_templates/edit-save/2', 'Update data Email Template Forgot Password Backend at Email Template', 1, '2018-04-24 21:06:25', NULL),
(231, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-24 21:06:34', NULL),
(232, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/forgot', 'Someone with IP ::1 request a password for rcaday0@gmail.com', NULL, '2018-04-24 21:33:09', NULL),
(233, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-24 21:34:10', NULL),
(234, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-24 21:35:09', NULL),
(235, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 21:35:24', NULL),
(236, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/users/edit-save/2', 'Update data Ryan James Caday at Users', 1, '2018-04-24 21:35:39', NULL),
(237, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-24 21:36:19', NULL),
(238, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-24 21:36:35', NULL),
(239, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/users/edit-save/2', 'Update data Ryan James Caday at Users', 2, '2018-04-24 21:36:59', NULL),
(240, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-24 21:37:02', NULL),
(241, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-24 21:37:36', NULL),
(242, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-24 21:41:03', NULL),
(243, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'admin@crudbooster.com login with IP Address ::1', 1, '2018-04-24 21:41:30', NULL),
(244, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/users/add-save', 'Add New Data Ryan James Caday at Users', 1, '2018-04-24 21:43:47', NULL),
(245, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'admin@crudbooster.com logout', 1, '2018-04-24 21:44:34', NULL),
(246, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-24 21:44:48', NULL),
(247, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/users/delete/1', 'Delete data Super Admin at Users', 3, '2018-04-24 21:45:02', NULL),
(248, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-24 21:45:07', NULL),
(249, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-24 21:52:22', NULL),
(250, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-24 21:53:10', NULL),
(251, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-24 21:53:45', NULL),
(252, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-24 21:54:12', NULL),
(253, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-24 21:54:25', NULL),
(254, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-24 22:01:38', NULL),
(255, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-24 22:02:11', NULL),
(256, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-24 22:03:20', NULL),
(257, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-24 22:03:35', NULL),
(258, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-24 22:06:21', NULL),
(259, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-24 22:06:43', NULL),
(260, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/logout', 'rcaday0@gmail.com logout', 2, '2018-04-24 22:26:42', NULL),
(261, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-24 22:26:53', NULL),
(262, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/documents/add-save', 'Add New Data as at Documents', 3, '2018-04-24 22:29:21', NULL),
(263, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 1 at Categories', 3, '2018-04-24 22:38:58', NULL),
(264, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 2 at Categories', 3, '2018-04-24 22:39:40', NULL),
(265, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 3 at Categories', 3, '2018-04-24 22:40:16', NULL),
(266, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 4 at Categories', 3, '2018-04-24 22:40:42', NULL),
(267, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 5 at Categories', 3, '2018-04-24 22:41:35', NULL),
(268, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 6 at Categories', 3, '2018-04-24 22:41:50', NULL),
(269, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 1 at Sub-Categories', 3, '2018-04-24 22:47:08', NULL),
(270, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 7 at Categories', 3, '2018-04-24 23:10:54', NULL),
(271, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/delete/7', 'Delete data 7 at Categories', 3, '2018-04-24 23:11:11', NULL),
(272, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/add-save', 'Add New Data 7 at Categories', 3, '2018-04-24 23:24:04', NULL),
(273, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/delete/7', 'Delete data 7 at Categories', 3, '2018-04-24 23:25:54', NULL),
(274, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 2 at Sub-Categories', 3, '2018-04-24 23:45:59', NULL),
(275, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 3 at Sub-Categories', 3, '2018-04-25 00:00:43', NULL),
(276, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/sub_categories/add-save', 'Add New Data 4 at Sub-Categories', 3, '2018-04-25 00:01:02', NULL),
(277, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/document_cats/edit-save/6', 'Update data  at Categories', 3, '2018-04-25 00:06:42', NULL),
(278, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/admin/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-25 17:17:33', NULL),
(279, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/5', 'Update data Documents at Menu Management', 3, '2018-04-25 17:19:16', NULL),
(280, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-25 17:19:33', NULL),
(281, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-25 17:20:02', NULL),
(282, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'rcaday0@gmail.com logout', 2, '2018-04-25 17:20:11', NULL),
(283, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-25 17:20:22', NULL),
(284, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/6', 'Update data Documents at Menu Management', 3, '2018-04-25 17:20:49', NULL),
(285, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-25 17:21:10', NULL),
(286, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'rcaday0@gmail.com login with IP Address ::1', 2, '2018-04-25 17:21:39', NULL),
(287, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'rcaday0@gmail.com logout', 2, '2018-04-25 17:22:43', NULL),
(288, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-25 17:22:57', NULL),
(289, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/8', 'Update data Documents at Menu Management', 3, '2018-04-25 17:24:02', NULL),
(290, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/12', 'Update data Categories at Menu Management', 3, '2018-04-25 17:24:44', NULL),
(291, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/9', 'Update data Category at Menu Management', 3, '2018-04-25 17:25:01', NULL),
(292, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data system admin at Users', 3, '2018-04-25 17:27:12', NULL),
(293, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-25 17:27:20', NULL),
(294, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-25 17:27:28', NULL),
(295, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-25 17:28:00', NULL),
(296, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-25 17:30:21', NULL),
(297, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/add-save', 'Add New Data 5 at Sub-Categories', 3, '2018-04-25 18:07:18', NULL),
(298, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/add-save', 'Add New Data 6 at Sub-Categories', 3, '2018-04-25 18:07:34', NULL),
(299, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete-image', 'Delete the image of 3 at Sub-Categories', 3, '2018-04-25 18:07:58', NULL),
(300, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/edit-save/3', 'Update data  at Sub-Categories', 3, '2018-04-25 18:08:44', NULL),
(301, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-25 18:10:24', NULL),
(302, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-25 18:10:36', NULL),
(303, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 00:45:19', NULL),
(304, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete-image', 'Delete the image of 1 at Sub-Categories', 4, '2018-04-26 00:46:50', NULL),
(305, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/main_categories/add-save', 'Add New Data 3 at Main Categories', 4, '2018-04-26 00:51:16', NULL),
(306, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cats/add-save', 'Add New Data 7 at Categories', 4, '2018-04-26 00:52:21', NULL),
(307, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/add-save', 'Add New Data 7 at Sub-Categories', 4, '2018-04-26 00:52:42', NULL),
(308, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/add-save', 'Add New Data 8 at Sub-Categories', 4, '2018-04-26 00:54:43', NULL),
(309, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-26 05:31:24', NULL),
(310, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/module_generator/delete/13', 'Delete data Documents at Module Generator', 3, '2018-04-26 05:35:20', NULL),
(311, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/10', 'Update data Panel at Menu Management', 3, '2018-04-26 05:35:47', NULL),
(312, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/9', 'Update data Main Category at Menu Management', 3, '2018-04-26 05:36:04', NULL),
(313, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/7', 'Delete data 7 at Sub-Categories', 3, '2018-04-26 05:38:34', NULL),
(314, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/8', 'Delete data 8 at Sub-Categories', 3, '2018-04-26 05:38:43', NULL),
(315, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/6', 'Delete data 6 at Sub-Categories', 3, '2018-04-26 05:38:46', NULL),
(316, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/5', 'Delete data 5 at Sub-Categories', 3, '2018-04-26 05:38:48', NULL),
(317, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/4', 'Delete data 4 at Sub-Categories', 3, '2018-04-26 05:38:51', NULL),
(318, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/3', 'Delete data 3 at Sub-Categories', 3, '2018-04-26 05:38:53', NULL),
(319, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/2', 'Delete data 2 at Sub-Categories', 3, '2018-04-26 05:38:56', NULL),
(320, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/delete/1', 'Delete data 1 at Sub-Categories', 3, '2018-04-26 05:38:58', NULL),
(321, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/add-save', 'Add New Data 1 at Sub-Categories', 3, '2018-04-26 05:41:14', NULL),
(322, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/sub_categories/add-save', 'Add New Data 2 at Sub-Categories', 3, '2018-04-26 05:41:30', NULL),
(323, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/main_categories/delete/3', 'Delete data 3 at Panel', 3, '2018-04-26 05:41:44', NULL),
(324, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/document_cats/delete/7', 'Delete data 7 at Main Categories', 3, '2018-04-26 05:41:49', NULL),
(325, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/9', 'Update data Main Category at Menu Management', 3, '2018-04-26 05:45:35', NULL),
(326, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/add-save', 'Add New Data QM-01 at Documents', 3, '2018-04-26 06:10:10', NULL),
(327, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/documents/add-save', 'Add New Data CP-01 at Documents', 3, '2018-04-26 07:00:48', NULL),
(328, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/delete/11', 'Delete data Sub-Categories at Menu Management', 3, '2018-04-26 07:30:54', NULL),
(329, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/module_generator/delete/16', 'Delete data Sub-Category at Module Generator', 3, '2018-04-26 07:31:44', NULL),
(330, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/module_generator/delete/14', 'Delete data Main Categories at Module Generator', 3, '2018-04-26 07:38:47', NULL),
(331, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/delete/5', 'Delete data Documents at Menu Management', 3, '2018-04-26 07:50:53', NULL),
(332, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/delete/6', 'Delete data Documents at Menu Management', 3, '2018-04-26 07:53:14', NULL),
(333, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/18', 'Update data Documents at Menu Management', 3, '2018-04-26 07:53:40', NULL),
(334, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-26 07:53:50', NULL),
(335, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 07:54:02', NULL),
(336, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-26 07:54:24', NULL),
(337, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-26 07:54:49', NULL),
(338, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/menu_management/edit-save/13', 'Update data Panel at Menu Management', 3, '2018-04-26 07:55:17', NULL),
(339, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-26 07:55:38', NULL),
(340, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 07:56:12', NULL),
(341, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-26 07:57:17', NULL),
(342, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-26 07:57:27', NULL),
(343, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-26 08:07:54', NULL),
(344, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 08:08:05', NULL),
(345, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-26 08:08:57', NULL),
(346, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-26 08:09:12', NULL),
(347, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-26 08:10:01', NULL),
(348, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 08:12:48', NULL),
(349, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-26 08:18:28', NULL),
(350, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-26 08:18:41', NULL),
(351, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 3, '2018-04-26 08:25:56', NULL),
(352, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/delete/5', 'Delete data test at Users', 3, '2018-04-26 08:26:39', NULL),
(353, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 3, '2018-04-26 08:26:59', NULL),
(354, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-26 08:27:55', NULL),
(355, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'test@noemail.com login with IP Address ::1', 5, '2018-04-26 08:28:04', NULL),
(356, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'test@noemail.com logout', 5, '2018-04-26 08:28:12', NULL),
(357, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-26 08:29:24', NULL),
(358, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/delete/5', 'Delete data test at Users', 3, '2018-04-26 08:29:35', NULL),
(359, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-26 08:50:37', NULL),
(360, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 08:50:46', NULL),
(361, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 4, '2018-04-26 08:59:44', NULL),
(362, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 4, '2018-04-26 09:05:15', NULL),
(363, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-26 09:05:37', NULL),
(364, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'test@noemail.com login with IP Address ::1', 5, '2018-04-26 09:05:49', NULL),
(365, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'test@noemail.com logout', 5, '2018-04-26 09:06:00', NULL),
(366, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 09:06:39', NULL),
(367, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 4, '2018-04-26 09:07:21', NULL),
(368, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 4, '2018-04-26 09:08:36', NULL),
(369, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/delete/5', 'Delete data test at Users', 4, '2018-04-26 09:08:46', NULL),
(370, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 4, '2018-04-26 09:09:01', NULL),
(371, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/delete/5', 'Delete data test at Users', 4, '2018-04-26 09:14:52', NULL),
(372, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data test at Users', 4, '2018-04-26 09:15:33', NULL),
(373, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/add-save', 'Add New Data testt at Users', 4, '2018-04-26 09:18:30', NULL),
(374, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-26 09:18:42', NULL),
(375, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'admin@noemail.com login with IP Address ::1', 4, '2018-04-26 09:19:10', NULL),
(376, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/delete/6', 'Delete data testt at Users', 4, '2018-04-26 09:19:39', NULL),
(377, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/users/delete/5', 'Delete data test at Users', 4, '2018-04-26 09:20:01', NULL),
(378, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'admin@noemail.com logout', 4, '2018-04-26 09:20:20', NULL),
(379, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/login', 'racaday@neda.gov.ph login with IP Address ::1', 3, '2018-04-26 09:20:35', NULL),
(380, '::1', 'Mozilla/5.0 (Windows NT 10.0; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36', 'http://localhost/qmsproj/public/qms/logout', 'racaday@neda.gov.ph logout', 3, '2018-04-26 09:21:02', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_menus`
--

CREATE TABLE `cms_menus` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'url',
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `parent_id` int(11) DEFAULT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_dashboard` tinyint(1) NOT NULL DEFAULT '0',
  `id_cms_privileges` int(11) DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_menus`
--

INSERT INTO `cms_menus` (`id`, `name`, `type`, `path`, `color`, `icon`, `parent_id`, `is_active`, `is_dashboard`, `id_cms_privileges`, `sorting`, `created_at`, `updated_at`) VALUES
(10, 'Panel', 'Route', 'AdminMainCategoriesControllerGetIndex', 'normal', 'fa fa-book', 0, 1, 0, 1, 1, '2018-04-24 01:08:38', '2018-04-26 05:35:47'),
(13, 'Panel', 'Route', 'AdminMainCategoriesControllerGetIndex', 'normal', 'fa fa-book', 0, 1, 0, 2, 2, '2018-04-24 21:53:06', '2018-04-26 07:55:17'),
(15, 'Documents', 'Route', 'AdminDocumentsControllerGetIndex', NULL, 'fa fa-file-pdf-o', 0, 1, 0, 1, 4, '2018-04-26 06:01:34', NULL),
(16, 'Sub-Category', 'Route', 'AdminSubCategoriesControllerGetIndex', NULL, 'fa fa-bookmark', 0, 1, 0, 1, 3, '2018-04-26 07:32:18', NULL),
(17, 'Main Category', 'Route', 'AdminDocumentCatsControllerGetIndex', NULL, 'fa fa-bookmark-o', 0, 1, 0, 1, 2, '2018-04-26 07:39:15', NULL),
(18, 'Documents', 'Route', 'AdminDocumentsControllerGetIndex', 'normal', 'fa fa-file-pdf-o', 0, 1, 1, 2, 1, '2018-04-26 07:53:03', '2018-04-26 07:53:40'),
(19, 'Main Category', 'Route', 'AdminDocumentCatsControllerGetIndex', 'normal', 'fa fa-bookmark-o', 0, 1, 0, 2, 3, '2018-04-26 07:53:03', NULL),
(20, 'Sub-Category', 'Route', 'AdminSubCategoriesControllerGetIndex', 'normal', 'fa fa-bookmark', 0, 1, 0, 2, 4, '2018-04-26 07:53:03', NULL),
(21, 'Users', 'Route', 'AdminCmsUsersControllerGetIndex', 'normal', 'fa fa-users', 0, 1, 0, 2, 5, '2018-04-26 08:07:50', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_moduls`
--

CREATE TABLE `cms_moduls` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `icon` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `table_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `controller` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_protected` tinyint(1) NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_moduls`
--

INSERT INTO `cms_moduls` (`id`, `name`, `icon`, `path`, `table_name`, `controller`, `is_protected`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Notifications', 'fa fa-cog', 'notifications', 'cms_notifications', 'NotificationsController', 1, 1, '2018-04-19 21:47:48', NULL),
(2, 'Privileges', 'fa fa-cog', 'privileges', 'cms_privileges', 'PrivilegesController', 1, 1, '2018-04-19 21:47:48', NULL),
(3, 'Privileges Roles', 'fa fa-cog', 'privileges_roles', 'cms_privileges_roles', 'PrivilegesRolesController', 1, 1, '2018-04-19 21:47:48', NULL),
(4, 'Users', 'fa fa-users', 'users', 'cms_users', 'AdminCmsUsersController', 0, 1, '2018-04-19 21:47:48', NULL),
(5, 'Settings', 'fa fa-cog', 'settings', 'cms_settings', 'SettingsController', 1, 1, '2018-04-19 21:47:48', NULL),
(6, 'Module Generator', 'fa fa-database', 'module_generator', 'cms_moduls', 'ModulsController', 1, 1, '2018-04-19 21:47:48', NULL),
(7, 'Menu Management', 'fa fa-bars', 'menu_management', 'cms_menus', 'MenusController', 1, 1, '2018-04-19 21:47:48', NULL),
(8, 'Email Template', 'fa fa-envelope-o', 'email_templates', 'cms_email_templates', 'EmailTemplatesController', 1, 1, '2018-04-19 21:47:48', NULL),
(9, 'Statistic Builder', 'fa fa-dashboard', 'statistic_builder', 'cms_statistics', 'StatisticBuilderController', 1, 1, '2018-04-19 21:47:48', NULL),
(10, 'API Generator', 'fa fa-cloud-download', 'api_generator', '', 'ApiCustomController', 1, 1, '2018-04-19 21:47:48', NULL),
(11, 'Logs', 'fa fa-flag-o', 'logs', 'cms_logs', 'LogsController', 1, 1, '2018-04-19 21:47:48', NULL),
(15, 'Panel', 'fa fa-book', 'main_categories', 'main_categories', 'AdminMainCategoriesController', 0, 0, '2018-04-24 01:08:38', NULL),
(17, 'Documents', 'fa fa-file-pdf-o', 'documents', 'documents', 'AdminDocumentsController', 0, 0, '2018-04-26 06:01:34', NULL),
(18, 'Sub-Category', 'fa fa-bookmark', 'sub_categories', 'sub_categories', 'AdminSubCategoriesController', 0, 0, '2018-04-26 07:32:18', NULL),
(19, 'Main Category', 'fa fa-bookmark-o', 'document_cats', 'document_cats', 'AdminDocumentCatsController', 0, 0, '2018-04-26 07:39:15', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_notifications`
--

CREATE TABLE `cms_notifications` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_users` int(11) DEFAULT NULL,
  `content` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_notifications`
--

INSERT INTO `cms_notifications` (`id`, `id_cms_users`, `content`, `url`, `is_read`, `created_at`, `updated_at`) VALUES
(16, 1, 'Array', 'http://localhost/qmsproj/public/admin/documents', 0, '2018-04-22 06:27:41', NULL),
(17, 1, '34', 'http://localhost/qmsproj/public/admin/documents', 0, '2018-04-22 06:28:40', NULL),
(18, 1, 'Super Admin added new category QMS023', 'http://localhost/qmsproj/public/admin/documents', 0, '2018-04-22 06:29:52', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges`
--

CREATE TABLE `cms_privileges` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_superadmin` tinyint(1) DEFAULT NULL,
  `theme_color` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges`
--

INSERT INTO `cms_privileges` (`id`, `name`, `is_superadmin`, `theme_color`, `created_at`, `updated_at`) VALUES
(1, 'Super Administrator', 1, 'skin-red', '2018-04-19 21:47:48', NULL),
(2, 'User', 0, 'skin-blue', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_privileges_roles`
--

CREATE TABLE `cms_privileges_roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `is_visible` tinyint(1) DEFAULT NULL,
  `is_create` tinyint(1) DEFAULT NULL,
  `is_read` tinyint(1) DEFAULT NULL,
  `is_edit` tinyint(1) DEFAULT NULL,
  `is_delete` tinyint(1) DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `id_cms_moduls` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_privileges_roles`
--

INSERT INTO `cms_privileges_roles` (`id`, `is_visible`, `is_create`, `is_read`, `is_edit`, `is_delete`, `id_cms_privileges`, `id_cms_moduls`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 0, 0, 0, 1, 1, '2018-04-19 21:47:48', NULL),
(2, 1, 1, 1, 1, 1, 1, 2, '2018-04-19 21:47:48', NULL),
(3, 0, 1, 1, 1, 1, 1, 3, '2018-04-19 21:47:48', NULL),
(4, 1, 1, 1, 1, 1, 1, 4, '2018-04-19 21:47:48', NULL),
(5, 1, 1, 1, 1, 1, 1, 5, '2018-04-19 21:47:48', NULL),
(6, 1, 1, 1, 1, 1, 1, 6, '2018-04-19 21:47:48', NULL),
(7, 1, 1, 1, 1, 1, 1, 7, '2018-04-19 21:47:48', NULL),
(8, 1, 1, 1, 1, 1, 1, 8, '2018-04-19 21:47:48', NULL),
(9, 1, 1, 1, 1, 1, 1, 9, '2018-04-19 21:47:48', NULL),
(10, 1, 1, 1, 1, 1, 1, 10, '2018-04-19 21:47:48', NULL),
(11, 1, 0, 1, 0, 1, 1, 11, '2018-04-19 21:47:48', NULL),
(12, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(13, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(14, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(15, 1, 1, 1, 1, 1, 1, 12, NULL, NULL),
(16, 1, 1, 1, 1, 1, 1, 13, NULL, NULL),
(17, 1, 1, 1, 1, 0, 2, 12, NULL, NULL),
(18, 1, 1, 1, 1, 1, 2, 13, NULL, NULL),
(19, 1, 1, 1, 1, 1, 1, 14, NULL, NULL),
(20, 1, 1, 1, 1, 1, 1, 15, NULL, NULL),
(21, 1, 1, 1, 1, 1, 1, 16, NULL, NULL),
(22, 1, 1, 1, 1, 1, 2, 4, NULL, NULL),
(23, 1, 1, 1, 1, 1, 2, 14, NULL, NULL),
(24, 1, 1, 1, 1, 1, 2, 15, NULL, NULL),
(25, 1, 1, 1, 1, 1, 2, 16, NULL, NULL),
(26, 1, 1, 1, 1, 1, 1, 17, NULL, NULL),
(27, 1, 1, 1, 1, 1, 1, 18, NULL, NULL),
(28, 1, 1, 1, 1, 1, 1, 19, NULL, NULL),
(29, 1, 1, 1, 1, 1, 2, 17, NULL, NULL),
(30, 1, 1, 1, 1, 1, 2, 19, NULL, NULL),
(31, 1, 1, 1, 1, 1, 2, 18, NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `cms_settings`
--

CREATE TABLE `cms_settings` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci,
  `content_input_type` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `dataenum` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `helper` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `group_setting` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `label` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_settings`
--

INSERT INTO `cms_settings` (`id`, `name`, `content`, `content_input_type`, `dataenum`, `helper`, `created_at`, `updated_at`, `group_setting`, `label`) VALUES
(1, 'login_background_color', NULL, 'text', NULL, 'Input hexacode', '2018-04-19 21:47:48', NULL, 'Login Register Style', 'Login Background Color'),
(2, 'login_font_color', NULL, 'text', NULL, 'Input hexacode', '2018-04-19 21:47:48', NULL, 'Login Register Style', 'Login Font Color'),
(3, 'login_background_image', 'uploads/2018-04/be15f617779676aea4ac88f5274a1c41.jpg', 'upload_image', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Login Register Style', 'Login Background Image'),
(4, 'email_sender', 'Icts_Admin', 'text', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Email Setting', 'Email Sender'),
(5, 'smtp_driver', 'smtp', 'select', 'smtp,mail,sendmail', NULL, '2018-04-19 21:47:48', NULL, 'Email Setting', 'Mail Driver'),
(6, 'smtp_host', 'smtp.gmail.com', 'text', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Email Setting', 'SMTP Host'),
(7, 'smtp_port', '465', 'text', NULL, 'default 25', '2018-04-19 21:47:48', NULL, 'Email Setting', 'SMTP Port'),
(8, 'smtp_username', 'ictsneda@gmail.com', 'text', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Email Setting', 'SMTP Username'),
(9, 'smtp_password', '1CT5@dministrator', 'text', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Email Setting', 'SMTP Password'),
(10, 'appname', 'QMS', 'text', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Application Setting', 'Application Name'),
(11, 'default_paper_size', 'A4', 'text', NULL, 'Paper size, ex : A4, Legal, etc', '2018-04-19 21:47:48', NULL, 'Application Setting', 'Default Paper Print Size'),
(12, 'logo', 'uploads/2018-04/227edfbe7bb02bd20c5afe58bf7e4266.jpg', 'upload_image', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Application Setting', 'Logo'),
(13, 'favicon', 'uploads/2018-04/567da449509c173a2571fc89d95e1148.jpg', 'upload_image', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Application Setting', 'Favicon'),
(14, 'api_debug_mode', 'true', 'select', 'true,false', NULL, '2018-04-19 21:47:48', NULL, 'Application Setting', 'API Debug Mode'),
(15, 'google_api_key', NULL, 'text', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Application Setting', 'Google API Key'),
(16, 'google_fcm_key', NULL, 'text', NULL, NULL, '2018-04-19 21:47:48', NULL, 'Application Setting', 'Google FCM Key');

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistics`
--

CREATE TABLE `cms_statistics` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_statistic_components`
--

CREATE TABLE `cms_statistic_components` (
  `id` int(10) UNSIGNED NOT NULL,
  `id_cms_statistics` int(11) DEFAULT NULL,
  `componentID` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `component_name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `area_name` varchar(55) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `sorting` int(11) DEFAULT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `config` longtext COLLATE utf8mb4_unicode_ci,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `cms_users`
--

CREATE TABLE `cms_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `photo` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `id_cms_privileges` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `status` varchar(50) COLLATE utf8mb4_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `cms_users`
--

INSERT INTO `cms_users` (`id`, `name`, `photo`, `email`, `password`, `id_cms_privileges`, `created_at`, `updated_at`, `status`) VALUES
(2, 'Ryan James Caday', 'uploads/2018-04/spansbab.jpeg', 'rcaday0@gmail.com', '$2y$10$dumMdTZeT9UqwVzUq/4DXO193VDadTjqoKd8slBGyS7wMu4qMAzk.', 2, '2018-04-19 23:07:42', '2018-04-24 21:36:59', NULL),
(3, 'Ryan James Caday', 'uploads/2018-04/popye.jpg', 'racaday@neda.gov.ph', '$2y$10$WiGblk5MeX/ZXwgv.37QDefyLq1euK.DoDPlRtAx6dalMJuU/OgFS', 1, '2018-04-24 21:43:47', NULL, NULL),
(4, 'system admin', 'uploads/2018-04/gupi.jpg', 'admin@noemail.com', '$2y$10$I6naYylGih4UdAtFJqa81OtaSXXxMUnUZKXGTxicWyGIZIPTGWTFG', 2, '2018-04-25 17:27:12', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `documents`
--

CREATE TABLE `documents` (
  `id` int(10) UNSIGNED NOT NULL,
  `doc_cat` int(11) DEFAULT NULL,
  `doc_sub_cat` int(11) DEFAULT NULL,
  `doc_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `attach` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remarks` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `documents`
--

INSERT INTO `documents` (`id`, `doc_cat`, `doc_sub_cat`, `doc_title`, `doc_code`, `attach`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 'QM-01', NULL, 'uploads/2018-04/sample-pdf-attachment.pdf', '', '2018-04-26 06:10:10', NULL),
(2, 4, 1, 'CP-01', NULL, 'uploads/2018-04/sample-atatsment-2.pdf', '', '2018-04-26 07:00:48', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `document_cat`
--

CREATE TABLE `document_cat` (
  `id` int(10) UNSIGNED NOT NULL,
  `category` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `document_cat`
--

INSERT INTO `document_cat` (`id`, `category`) VALUES
(1, 'Quality Manual'),
(2, 'Quality Manual Annex'),
(3, 'Quality System Procedures'),
(4, 'Quality Operating Procedures'),
(5, 'Core Process'),
(6, 'Support Process'),
(7, 'Certification of the Head of the Agency on the Conduct of Internal Quality Audit'),
(8, 'QMS023'),
(9, 'test'),
(10, 'test1');

-- --------------------------------------------------------

--
-- Table structure for table `document_cats`
--

CREATE TABLE `document_cats` (
  `id` int(10) UNSIGNED NOT NULL,
  `maincat_id` int(10) UNSIGNED NOT NULL,
  `category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_title` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_code` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_attach` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `document_cats`
--

INSERT INTO `document_cats` (`id`, `maincat_id`, `category`, `doc_title`, `doc_code`, `doc_attach`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 1, 'Quality Manual', '', 'QM', NULL, '', '2018-04-24 22:38:58', NULL),
(2, 1, 'Quality Manual Annexes', 'QM-Annex', '', NULL, '', '2018-04-24 22:39:40', NULL),
(3, 1, 'Quality System Procedures', '', 'QSP', NULL, '', '2018-04-24 22:40:16', NULL),
(4, 1, 'Quality Operating Procedures', '', 'QOP', NULL, '', '2018-04-24 22:40:42', NULL),
(5, 2, 'Certification of the Head of the Agency on the Conduct of Internal Quality Audit', '', '', NULL, '', '2018-04-24 22:41:35', NULL),
(6, 2, 'Minutes of Management Review', '', '', 'uploads/2018-04/sample-pdf-attachment.pdf', '', '2018-04-24 22:41:50', '2018-04-25 00:06:42');

-- --------------------------------------------------------

--
-- Table structure for table `main_categories`
--

CREATE TABLE `main_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `main_category` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `main_categories`
--

INSERT INTO `main_categories` (`id`, `main_category`, `created_at`, `updated_at`) VALUES
(1, 'ISO 9001 - Aligned QMS Documents', '2018-04-24 00:48:24', '2018-04-24 00:48:24'),
(2, 'Evidence of ISO 9001 - Aligned QMS Implementation', '2018-04-24 00:49:08', '2018-04-24 00:49:08');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2016_08_07_145904_add_table_cms_apicustom', 1),
(2, '2016_08_07_150834_add_table_cms_dashboard', 1),
(3, '2016_08_07_151210_add_table_cms_logs', 1),
(4, '2016_08_07_152014_add_table_cms_privileges', 1),
(5, '2016_08_07_152214_add_table_cms_privileges_roles', 1),
(6, '2016_08_07_152320_add_table_cms_settings', 1),
(7, '2016_08_07_152421_add_table_cms_users', 1),
(8, '2016_08_07_154624_add_table_cms_moduls', 1),
(9, '2016_08_17_225409_add_status_cms_users', 1),
(10, '2016_08_20_125418_add_table_cms_notifications', 1),
(11, '2016_09_04_033706_add_table_cms_email_queues', 1),
(12, '2016_09_16_035347_add_group_setting', 1),
(13, '2016_09_16_045425_add_label_setting', 1),
(14, '2016_09_17_104728_create_nullable_cms_apicustom', 1),
(15, '2016_10_01_141740_add_method_type_apicustom', 1),
(16, '2016_10_01_141846_add_parameters_apicustom', 1),
(17, '2016_10_01_141934_add_responses_apicustom', 1),
(18, '2016_10_01_144826_add_table_apikey', 1),
(19, '2016_11_14_141657_create_cms_menus', 1),
(20, '2016_11_15_132350_create_cms_email_templates', 1),
(21, '2016_11_15_190410_create_cms_statistics', 1),
(22, '2016_11_17_102740_create_cms_statistic_components', 1),
(26, '2018_04_24_084222_create_main_categories_table', 5),
(30, '2018_04_23_145510_create_document_cats_table', 6),
(31, '2018_04_24_065843_create_sub_categories_table', 6),
(32, '2018_04_26_135117_create_documents_table', 7);

-- --------------------------------------------------------

--
-- Table structure for table `sub_categories`
--

CREATE TABLE `sub_categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `cat_id` int(10) UNSIGNED NOT NULL,
  `subcategory` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `doc_title` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_code` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `doc_attach` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `remarks` varchar(191) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `sub_categories`
--

INSERT INTO `sub_categories` (`id`, `cat_id`, `subcategory`, `doc_title`, `doc_code`, `doc_attach`, `remarks`, `created_at`, `updated_at`) VALUES
(1, 4, 'Core Process', NULL, NULL, NULL, NULL, '2018-04-26 05:41:13', NULL),
(2, 4, 'Support Process', NULL, NULL, NULL, NULL, '2018-04-26 05:41:30', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_logs`
--
ALTER TABLE `cms_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_menus`
--
ALTER TABLE `cms_menus`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_settings`
--
ALTER TABLE `cms_settings`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cms_users`
--
ALTER TABLE `cms_users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `documents`
--
ALTER TABLE `documents`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_cat`
--
ALTER TABLE `document_cat`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `document_cats`
--
ALTER TABLE `document_cats`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `main_categories`
--
ALTER TABLE `main_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sub_categories`
--
ALTER TABLE `sub_categories`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `cms_apicustom`
--
ALTER TABLE `cms_apicustom`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_apikey`
--
ALTER TABLE `cms_apikey`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_dashboard`
--
ALTER TABLE `cms_dashboard`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_queues`
--
ALTER TABLE `cms_email_queues`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_email_templates`
--
ALTER TABLE `cms_email_templates`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_logs`
--
ALTER TABLE `cms_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=381;

--
-- AUTO_INCREMENT for table `cms_menus`
--
ALTER TABLE `cms_menus`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;

--
-- AUTO_INCREMENT for table `cms_moduls`
--
ALTER TABLE `cms_moduls`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT for table `cms_notifications`
--
ALTER TABLE `cms_notifications`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT for table `cms_privileges`
--
ALTER TABLE `cms_privileges`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `cms_privileges_roles`
--
ALTER TABLE `cms_privileges_roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=32;

--
-- AUTO_INCREMENT for table `cms_settings`
--
ALTER TABLE `cms_settings`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `cms_statistics`
--
ALTER TABLE `cms_statistics`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_statistic_components`
--
ALTER TABLE `cms_statistic_components`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `cms_users`
--
ALTER TABLE `cms_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `documents`
--
ALTER TABLE `documents`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `document_cat`
--
ALTER TABLE `document_cat`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT for table `document_cats`
--
ALTER TABLE `document_cats`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `main_categories`
--
ALTER TABLE `main_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=33;

--
-- AUTO_INCREMENT for table `sub_categories`
--
ALTER TABLE `sub_categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
